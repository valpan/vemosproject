import { Response } from './../../../../shared/response';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from 'src/app/services/user.service';
import { DiscussionService } from './../../../../services/discussion.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DiscussionDetailCardObject } from 'src/app/shared/gui-interfaces/discussionDetailCardModel';

@Component({
  selector: 'app-answer-card',
  templateUrl: './answer-card.component.html',
  styleUrls: ['./answer-card.component.css']
})
export class AnswerCardComponent implements OnInit {
  @Input() card: DiscussionDetailCardObject;
  @Output() updateDiscussion: EventEmitter<Object> = new EventEmitter();
  isEditClicked: boolean = false;

  isCreator: boolean = false;
  constructor(
    private discussionService: DiscussionService,
    private userService: UserService,
    private snackbar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.isCreator = this.card.creator._id === this.userService.getUser()._id;
  }

  like() {
    this.discussionService
      .likeComment(this.card.parentId, this.card.id, this.userService.getUser()._id)
      .subscribe((data: Response) => {
        if (data.success) {
          this.updateDiscussion.emit(data.object);
        }
      });
  }

  toggleEdit() {
    this.isEditClicked = !this.isEditClicked;
  }

  submitEdit() {
    this.discussionService
      .editComment(this.card.parentId, this.card.id, this.card.mainText)
      .subscribe((response: Response) => {
        this.snackbar.open(response.message, 'Remove', {
          duration: 5000
        });
      });

    this.toggleEdit();
  }

  deleteComment() {
    this.discussionService
      .deleteComment(this.card.parentId, this.card.id)
      .subscribe((response: Response) => {
        if (response.success) {
          this.updateDiscussion.emit(response.object);
        }
        this.snackbar.open(response.message, 'Remove', {
          duration: 5000
        });
      });
  }

  isLiked() {
    return this.card.likedBy?.includes(this.userService.getUser()._id);
  }
}
