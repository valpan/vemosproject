import { Notification } from './../../../../shared/notification';
import { Response } from './../../../../shared/response';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { DiscussionService } from './../../../../services/discussion.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DiscussionDetailCardObject } from 'src/app/shared/gui-interfaces/discussionDetailCardModel';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-question-card',
  templateUrl: './question-card.component.html',
  styleUrls: ['./question-card.component.css']
})
export class QuestionCardComponent implements OnInit {
  @Input() card: DiscussionDetailCardObject;
  @Output() updateDiscussion: EventEmitter<Object> = new EventEmitter();
  isCreator: boolean = false;
  isReplyClicked: boolean = false;
  isEditClicked: boolean = false;
  commentText: string;

  constructor(
    private discussionService: DiscussionService,
    private userService: UserService,
    private notificationService: NotificationService,
    private router: Router,
    private snackbar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.isCreator = this.card.creator._id === this.userService.getUser()._id;
  }

  like() {
    this.discussionService
      .likeDiscussion(this.card.id, this.userService.getUser()._id)
      .subscribe((data: Response) => {
        if (data.success) {
          this.updateDiscussion.emit(data.object);
        }
      });
  }

  toggleComment() {
    this.isReplyClicked = !this.isReplyClicked;
  }

  submitComment() {
    let newComment = {
      discussionId: this.card.id,
      commentText: this.commentText,
      commentCreator: {
        id: this.userService.getUser()._id,
        firstName: this.userService.getUser().firstName,
        lastName: this.userService.getUser().lastName,
        username: this.userService.getUser().username
      }
    };

    this.discussionService.createComment(newComment).subscribe((response: Response) => {
      if (response.success) {
        this.updateDiscussion.emit(response.object);
        if (this.card.creator._id !== this.userService.getUser()._id) {
          let newNotification: Notification = {
            toUser: this.card.creator._id,
            fromUser: this.userService.getUser()._id,
            discussionId: this.card.id,
            requestType: 'comment'
          };
          this.notificationService.createNotification(newNotification).subscribe();
        }
      }
      this.snackbar.open(response.message, 'Remove', {
        duration: 5000
      });
    });
    this.commentText = '';
    this.toggleComment();
  }

  toggleEdit() {
    this.isEditClicked = !this.isEditClicked;
  }

  submitEdit() {
    this.discussionService
      .editQuestion(this.card.id, this.card.mainText)
      .subscribe((response: Response) => {
        this.snackbar.open(response.message, 'Remove', {
          duration: 5000
        });
      });
    this.toggleEdit();
  }

  deleteQuestion() {
    this.discussionService.deleteDiscussion(this.card.id).subscribe();
    this.router.navigate(['/course/', this.card.course]);
  }

  isLiked() {
    return this.card.likedBy?.includes(this.userService.getUser()._id);
  }
}
