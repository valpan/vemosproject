import { MatSnackBar } from '@angular/material/snack-bar';
import { Response } from './../../../shared/response';
import { UserService } from './../../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CourseService } from 'src/app/services/course.service';
import { Course } from 'src/app/shared/course';
import { Location } from '@angular/common';

@Component({
  selector: 'app-selected-course',
  templateUrl: './selected-course.component.html',
  styleUrls: ['./selected-course.component.css']
})
export class SelectedCourseComponent implements OnInit {
  course: Course;
  isJoined: boolean;

  constructor(
    private route: ActivatedRoute,
    private courseService: CourseService,
    private userService: UserService,
    private location: Location,
    private snackbar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.loadCourse();
    console.log('course', this.course);
  }

  loadCourse() {
    const response: Response = this.route.snapshot.data.resData;
    if (response.success) {
      this.course = this.route.snapshot.data.resData.object;
      this.setIsJoined();
    } else {
      this.snackbar
        .open(response.message, 'Remove', {
          duration: 5000
        })
        .afterDismissed()
        .subscribe(() => {
          this.location.back();
        });
    }
  }

  joinCourse() {
    this.courseService
      .joinCourseMember(this.course._id, this.userService.getUser()._id)
      .subscribe((response: Response) => {
        if (response.success) {
          this.course = response.object;
          this.setIsJoined();
        }
        this.snackbar.open(response.message, 'Remove', {
          duration: 5000
        });
      });
  }

  leaveCourse() {
    this.courseService
      .leaveCourseMember(this.course._id, this.userService.getUser()._id)
      .subscribe((response: Response) => {
        if (response.success) {
          this.course = response.object;
          this.setIsJoined();
        }
        this.snackbar.open(response.message, 'Remove', {
          duration: 5000
        });
      });
  }

  setIsJoined() {
    if (this.course.members.some((e) => e._id === this.userService.getUser()._id)) {
      this.isJoined = true;
    } else {
      this.isJoined = false;
    }
  }
}
