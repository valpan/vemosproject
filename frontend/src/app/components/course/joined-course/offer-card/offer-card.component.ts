import { UserService } from './../../../../services/user.service';
import { NotificationService } from './../../../../services/notification.service';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/shared/user';
import { Course } from 'src/app/shared/course';
import { Notification } from 'src/app/shared/notification';
import { Response } from '../../../../shared/response';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-offer-card',
  templateUrl: './offer-card.component.html',
  styleUrls: ['./offer-card.component.css']
})
export class OfferCardComponent {
  @Input() person: User;
  @Input() course: Course;
  @Input() tutorRate: Number;
  @Input() isPal: boolean;
  @Input() myPals: Notification[];
  @Input() myTutors: Notification[];
  @Output() reloadList: EventEmitter<Object> = new EventEmitter();
  buttonText: string = 'Request';

  constructor(
    private notificationService: NotificationService,
    private userService: UserService,
    private snackbar: MatSnackBar
  ) {}

  getRating() {
    if (this.isPal) {
      return this.getPalRating();
    } else {
      return this.getTutorRating();
    }
  }

  getPalRating() {
    return (
      this.person.palRatings?.reduce((accumulator, current) => accumulator + current.rating, 0) /
      this.person.palRatings?.length
    );
  }

  getTutorRating() {
    return (
      this.person.tutorRatings?.reduce((accumulator, current) => accumulator + current.rating, 0) /
      this.person.tutorRatings?.length
    );
  }

  isRequestButtonDisabled() {
    let foundItem = null;
    if (this.isPal) {
      foundItem = this.myPals?.find(
        (e) => e.toUser['_id'] === this.person._id || e.fromUser['_id'] === this.person._id
      );
    } else {
      foundItem = this.myTutors?.find(
        (e) => e.toUser['_id'] === this.person._id || e.fromUser['_id'] === this.person._id
      );
    }

    if (this.person._id === this.userService.getUser()._id) {
      this.buttonText = 'You';
      return true;
    }

    if (foundItem) {
      if (foundItem.status === 'accepted') {
        this.buttonText = 'Connected';
      } else {
        this.buttonText = 'Pending';
      }
      return true;
    }
    return false;
  }

  request() {
    let newNotification: Notification = {
      toUser: this.person._id,
      fromUser: this.userService.getUser()._id,
      course: { _id: this.course._id, name: this.course.name },
      requestType: ''
    };
    if (this.isPal) {
      newNotification.requestType = 'pal';
    } else {
      newNotification.requestType = 'tutor';
    }
    this.notificationService.createNotification(newNotification).subscribe((response: Response) => {
      if (response.success) {
        this.reloadList.emit();
      }
      this.snackbar.open(response.message, 'Remove', {
        duration: 5000
      });
    });
  }
}
