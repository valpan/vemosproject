import { Notification } from 'src/app/shared/notification';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { CourseService } from 'src/app/services/course.service';
import { Course } from '../../shared/course';
import { User } from 'src/app/shared/user';
import { NotificationService } from 'src/app/services/notification.service';
import { Response } from 'src/app/shared/response';

@Component({
  selector: 'app-personal-profile',
  templateUrl: './personal-profile.component.html',
  styleUrls: ['./personal-profile.component.css']
})
export class PersonalProfileComponent implements OnInit {
  currentUser: User;
  requestsToUser: Notification[];
  requestsFromUser: Notification[];
  selected: 'pal' | 'tutor' = 'pal';
  selectedCourse: Course;
  courseList: Course[];
  profilePictureName: string;

  constructor(
    private userService: UserService,
    private courseService: CourseService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.load();
    this.loadRequests();
  }

  checkIfExist() {
    if (this.selectedCourse && this.selected === 'pal') {
      return this.selectedCourse.pals.some((pal) => pal._id === this.currentUser._id);
    } else if (this.selectedCourse && this.selected === 'tutor') {
      return this.selectedCourse.tutors.some((tutor) => tutor._id === this.currentUser._id);
    }
    return true;
  }

  filterByCourse(course: Course) {
    this.selectedCourse = course;
  }

  async submit(rate?: number) {
    if (this.selected === 'tutor') {
      await this.courseService.joinCourseTutor(this.selectedCourse._id, this.currentUser._id, rate);
    } else {
      await this.courseService.joinCoursePal(this.selectedCourse._id, this.currentUser._id);
    }
    this.ngOnInit();
  }

  load() {
    this.currentUser = this.userService.getUser();
    this.userService.findUser(this.currentUser._id).subscribe((response: Response) => {
      if (response.success) {
        this.currentUser = response.object;
      }
    });
    this.courseService.getMyCoursesMember(this.currentUser._id).subscribe((res) => {
      if (res.success) {
        this.courseList = res.object;
      }
    });
  }

  updateProfile() {
    this.userService.updateUser(this.currentUser);
  }

  private loadRequests() {
    this.notificationService.getNotificationsTo(this.currentUser._id).subscribe((requests) => {
      this.requestsToUser = requests.objectArray;
    });
    this.notificationService.getNotificationsFrom(this.currentUser._id).subscribe((requests) => {
      this.requestsFromUser = requests.objectArray;
    });
  }

  getPalRating() {
    return (
      this.currentUser.palRatings?.reduce(
        (accumulator, current) => accumulator + current.rating,
        0
      ) / this.currentUser.palRatings?.length
    );
  }

  getTutorRating() {
    return (
      this.currentUser.tutorRatings?.reduce(
        (accumulator, current) => accumulator + current.rating,
        0
      ) / this.currentUser.tutorRatings?.length
    );
  }
}
