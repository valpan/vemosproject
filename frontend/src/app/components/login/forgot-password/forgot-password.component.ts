import { Response } from './../../../shared/response';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Component } from '@angular/core';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  email: string = '';
  password: string = '';
  isVerified: boolean = false;
  userId: string;

  constructor(
    private router: Router,
    private snackbar: MatSnackBar,
    private userService: UserService
  ) {}

  emailSubmitButton() {
    this.userService.checkIfAccountExists(this.email).subscribe((response: Response) => {
      if (response.success) {
        this.userId = response.object._id;
        this.isVerified = true;
        this.snackbar.open('User found', 'Remove', {
          duration: 5000
        });
      }
    });
    this.email = '';
  }

  passwordSubmitButton() {
    this.userService.resetPassword(this.userId, this.password).subscribe((response: Response) => {
      if (response.success) {
        this.snackbar
          .open('Password resetted', 'Remove', {
            duration: 5000
          })
          .afterDismissed()
          .subscribe(() => {
            this.router.navigate(['login']);
          });
      }
    });
  }
}
