import { Component, Input, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Course } from 'src/app/shared/course';
import { University } from 'src/app/shared/university';

@Component({
  selector: 'app-course-tab',
  templateUrl: './course-tab.component.html',
  styleUrls: ['./course-tab.component.css']
})
export class CourseTabComponent implements OnInit {
  @Input() courses: Course[];
  @Input() uniList: University[];
  coursesShown: Course[];
  currentPage: number = 0;
  pageSize: number = 5;

  constructor() {}

  ngOnInit() {
    this.updateCommentsArray();
  }

  filterByUni(uni: University) {
    if (this.coursesShown) {
      this.coursesShown = this.courses.map((x) => Object.assign({}, x));
    }
    this.courses = this.courses.map((x) => Object.assign({}, x));
    this.coursesShown = this.coursesShown.filter((course) => course.university.name === uni.name);
  }

  sort(value: string) {
    if (value === 'sortAZ') {
      return this.coursesShown.sort((a, b) => a.name.toLowerCase().localeCompare(b.name));
    } else {
      return this.coursesShown.sort((a, b) => b.name.toLowerCase().localeCompare(a.name));
    }
  }

  paginatorButton(pageEvent: PageEvent) {
    this.currentPage = pageEvent.pageIndex;
    this.pageSize = pageEvent.pageSize;
    this.updateCommentsArray();
  }

  updateCommentsArray() {
    const start = this.currentPage * this.pageSize;
    const end = (this.currentPage + 1) * this.pageSize;
    this.coursesShown = this.courses.slice(start, end);
  }
}
