import { UserService } from './../../services/user.service';
import { University } from './../../shared/university';
import { CourseService } from './../../services/course.service';
import { Course } from './../../shared/course';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-courses',
  templateUrl: './my-courses.component.html',
  styleUrls: ['./my-courses.component.css']
})
export class MyCoursesComponent implements OnInit {
  memberCourses: Course[];
  palCourses: Course[];
  tutorCourses: Course[];
  uniList: University[];
  memberCoursesShown: Course[];
  palCoursesShown: Course[];
  tutorCoursesShown: Course[];

  constructor(private userService: UserService, private courseService: CourseService) {}

  ngOnInit(): void {
    this.loadCourses(this.userService.getUser()._id);
    this.loadUniList();
  }

  loadCourses(userId: string) {
    this.courseService.getMyCoursesMember(userId).subscribe((res) => {
      if (res.success) {
        this.memberCourses = res.object;
      }
    });
    this.courseService.getMyCoursesPal(userId).subscribe((res) => {
      if (res.success) {
        this.palCourses = res.object;
      } else {
      }
    });
    this.courseService.getMyCoursesTutor(userId).subscribe((res) => {
      if (res.success) {
        this.tutorCourses = res.object;
      }
    });
  }

  async loadUniList() {
    this.uniList = await this.courseService.getUniList().then((uniList) => uniList);
  }
}
