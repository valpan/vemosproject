import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.css']
})
export class ProfileSettingsComponent implements OnInit {
  @Output() loggedHandler = new EventEmitter<boolean>();

  constructor(private userService: UserService) {}

  ngOnInit(): void {}
  logout() {
    this.loggedHandler.emit(true);
    this.userService.logout();
  }
}
