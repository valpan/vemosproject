import { NotificationService } from './../../services/notification.service';
import { Notification } from './../../shared/notification';
import { UserService } from './../../services/user.service';
import { Router } from '@angular/router';
import { Component, HostListener, OnInit } from '@angular/core';
import { User } from 'src/app/shared/user';

@Component({
  selector: 'app-navigator',
  templateUrl: './navigator.component.html',
  styleUrls: ['./navigator.component.css']
})
export class NavigatorComponent implements OnInit {
  searchInput: String;
  isCollapsed = true;
  isOpen = false;
  isLeftBarOpen = false;
  isNeither = true;
  isNotificationsToggled = false;
  isSettignsToggled = false;
  isNotifications: boolean;
  counterNewNotifications: number;
  notifications: Notification[];
  isLoggedOut: boolean = false;
  user: User;
  focus: boolean = false;

  constructor(
    private router: Router,
    protected userService: UserService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.user = this.userService.getUser();
    this.refreshNotifications();
    setInterval(() => {
      this.refreshNotifications();
    }, 10 * 1000);
    this.userService.componentMethodCallSource.subscribe(() => {
      this.refreshNotifications();
      this.user = this.userService.getUser();
    });
    this.notificationService.updatedEvent.subscribe((value) => {
      if (value === true) {
        this.refreshNotifications();
      }
    });
  }

  @HostListener('document:keyup.esc')
  onkeyup() {
    this.isOpen = false;
  }

  @HostListener('keydown.enter') onEnterKeyDown(event: KeyboardEvent) {
    if (this.focus) {
      this.executeSearch();
    }
  }

  getNewCounter(count: number) {
    this.counterNewNotifications = count;
  }

  executeSearch() {
    this.router.navigateByUrl('/results?search_query=' + this.searchInput);
  }

  chooseAction() {
    if (this.isCollapsed) {
      this.openNav();
    } else {
      this.closeNav();
    }
  }
  openNav() {
    this.isCollapsed = false;
    document.getElementById('mySidebar').style.width = '350px';
    document.getElementById('main').style.marginLeft = '350px';
    document.getElementById('btn').setAttribute('align-self', 'center');
  }
  closeNav() {
    this.isCollapsed = true;
    document.getElementById('mySidebar').style.width = '50px';
    document.getElementById('main').style.marginLeft = '50px';
    document.getElementById('btn').setAttribute('align-self', 'center');
  }
  toggleNotifications() {
    this.isNotifications = true;
    if (this.isNeither) {
      this.isOpen = true;
      this.isNotificationsToggled = true;
      this.isNeither = false;
    } else if (this.isNotificationsToggled) {
      this.isOpen = false;
      this.isNeither = true;
      this.isNotificationsToggled = false;
    } else if (this.isSettignsToggled) {
      this.isOpen = true;
      this.isNeither = false;
      this.isNotificationsToggled = true;
      this.isSettignsToggled = false;
    }
  }
  toggleAccountSettings() {
    this.isNotifications = false;
    if (this.isNeither) {
      this.isOpen = true;
      this.isSettignsToggled = true;
      this.isNeither = false;
    } else if (this.isSettignsToggled) {
      this.isOpen = false;
      this.isSettignsToggled = false;
      this.isNeither = true;
    } else if (this.isNotificationsToggled) {
      this.isOpen = true;
      this.isNeither = false;
      this.isNotificationsToggled = false;
      this.isSettignsToggled = true;
    }
  }

  isLoggedIn() {
    return this.userService.getUser() != null;
  }
  getLoggedHandler(isTrue: boolean) {
    this.isOpen = !isTrue;
    this.isNeither = true;
    this.isSettignsToggled = false;
    this.isLeftBarOpen = false;
  }
  refreshNotifications() {
    if (this.isLoggedIn()) {
      this.notificationService
        .getNotifications(this.userService.getUser()._id)
        .subscribe((result) => {
          if (result.objectArray?.type != 'undefined') {
            this.notifications = result.objectArray;
            this.counterNewNotifications = 0;
            this.notifications?.forEach((notification) => {
              if (!notification.isRead) {
                this.counterNewNotifications++;
              }
            });
          }
        });
    }
  }
  resetSidebars() {
    this.isOpen = false;
    this.isNeither = true;
    this.isNotificationsToggled = false;
    this.isSettignsToggled = false;
  }
}
