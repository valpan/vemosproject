import { User } from 'src/app/shared/user';
import { Notification } from 'src/app/shared/notification';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from './../../../services/user.service';
import { NotificationService } from './../../../services/notification.service';
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  @Input() notifications: Notification[];
  @Input() counterNewNotifications: number;
  @Output() refreshNotifications = new EventEmitter<number>();
  opened: boolean;

  constructor(
    private notificationService: NotificationService,
    private snackbar: MatSnackBar,
    private userService: UserService
  ) {}

  acceptRequest(notificationId: string, fromUser: User) {
    this.notificationService.answerNotification(notificationId, 'accepted').subscribe((data) => {
      this.refreshNotifications.emit();
      this.snackbar.open('Request accepted!', 'Remove', { duration: 5000 });
      this.notificationService.updateIncomingRequests(true);
    });
    const newNotification: Notification = {
      toUser: fromUser._id,
      fromUser: this.userService.getUser()._id,
      status: 'accepted',
      requestType: 'response'
    };
    this.notificationService.createNotification(newNotification).subscribe(() => {});
  }
  rejectRequest(notificationId: string, fromUser: User) {
    this.notificationService.answerNotification(notificationId, 'rejected').subscribe((data) => {
      this.refreshNotifications.emit();
      this.snackbar.open('Request declined!', 'Remove', { duration: 5000 });
      this.notificationService.updateIncomingRequests(true);
    });
    const newNotification: Notification = {
      toUser: fromUser._id,
      fromUser: this.userService.getUser()._id,
      status: 'rejected',
      requestType: 'response'
    };
    this.notificationService.createNotification(newNotification).subscribe(() => {});
  }
  readNotification(notificationId: string) {
    this.notificationService.readNotification(notificationId, true).subscribe((data) => {
      this.refreshNotifications.emit();
      this.notificationService.updateIncomingRequests(true);
    });
  }
  isLoggedIn() {
    return this.userService.getUser() != null;
  }
  ngOnInit(): void {}
}
