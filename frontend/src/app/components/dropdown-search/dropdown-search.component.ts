import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { Course } from '../../shared/course';
import { University } from '../../shared/university';

@Component({
  selector: 'app-dropdown-search',
  templateUrl: './dropdown-search.component.html',
  styleUrls: ['./dropdown-search.component.css']
})
export class DropdownSearchComponent implements OnInit {
  /** list of Uni or Course list */
  @Input() list: (University | Course)[];
  @Input() placeholder: string;
  @Output()
  filterBy: EventEmitter<University> = new EventEmitter<University>();

  /** control for the selected uni */
  public ctrl: FormControl = new FormControl();

  /** control for the MatSelect filter keyword */
  public filterCtrl: FormControl = new FormControl();

  /** list of Uni or Course list filtered by search keyword */
  public filtereduniversity: ReplaySubject<any> = new ReplaySubject<string>(1);

  @ViewChild('singleSelect') singleSelect: MatSelect;

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();

  constructor() {}

  ngOnInit() {
    if (this.list) {
      // set initial selection
      this.ctrl.setValue(this.list[10]);

      // load the initial uni list
      this.filtereduniversity.next(this.list.slice());
    }

    // listen for search field value changes
    this.filterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => {
      this.filteruniversity();
    });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  onSelect(form) {
    this.filterBy.emit(form.value);
  }

  /**
   * Sets the initial value after the filtereduniversity are loaded initially
   */
  protected setInitialValue() {
    this.filtereduniversity.pipe(take(1), takeUntil(this._onDestroy)).subscribe(() => {
      // setting the compareWith property to a comparison function
      // triggers initializing the selection according to the initial value of
      // the form control (i.e. _initializeSelection())
      // this needs to be done after the filtereduniversity are loaded initially
      // and after the mat-option elements are available
      this.singleSelect.compareWith = (a: University, b: University) => a && b && a.id === b.id;
    });
  }

  protected filteruniversity() {
    if (!this.list) {
      return;
    }
    // get the search keyword
    let search = this.filterCtrl.value;
    if (!search) {
      this.filtereduniversity.next(this.list.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the list
    this.filtereduniversity.next(
      this.list.filter((uni) => uni.name.toLowerCase().indexOf(search) > -1)
    );
  }
}
