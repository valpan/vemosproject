import { Notification } from './../../shared/notification';
import { Response } from './../../shared/response';
import { NotificationService } from './../../services/notification.service';
import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-pals-tutors',
  templateUrl: './pals-tutors.component.html',
  styleUrls: ['./pals-tutors.component.css']
})
export class PalsTutorsComponent implements OnInit {
  palConnections: Notification[];
  tutorConnections: Notification[];
  tuteeConnections: Notification[];

  constructor(private userService: UserService, private notificationService: NotificationService) {}
  ngOnInit(): void {
    this.loadPalConnections();
    this.loadTutorConnections();
    this.loadTuteeConnections();
  }

  loadPalConnections() {
    this.notificationService
      .getMyPals(this.userService.getUser()._id)
      .subscribe((response: Response) => {
        if (response.success) {
          this.palConnections = response.object;
        }
      });
  }

  loadTutorConnections() {
    this.notificationService
      .getMyTutors(this.userService.getUser()._id)
      .subscribe((response: Response) => {
        if (response.success) {
          this.tutorConnections = response.object;
        }
      });
  }
  loadTuteeConnections() {
    this.notificationService
      .getMyTutees(this.userService.getUser()._id)
      .subscribe((response: Response) => {
        if (response.success) {
          this.tuteeConnections = response.object;
        }
      });
  }
}
