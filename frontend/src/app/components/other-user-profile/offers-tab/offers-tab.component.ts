import { MatSnackBar } from '@angular/material/snack-bar';
import { Response } from './../../../shared/response';
import { NotificationService } from 'src/app/services/notification.service';
import { UserService } from 'src/app/services/user.service';
import { Notification } from 'src/app/shared/notification';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Course } from 'src/app/shared/course';
import { User } from 'src/app/shared/user';

@Component({
  selector: 'app-offers-tab',
  templateUrl: './offers-tab.component.html',
  styleUrls: ['./offers-tab.component.css']
})
export class OffersTabComponent {
  @Input() user: User;
  @Input() courses: Course[];
  @Input() rating: number;
  @Input() isPal: boolean;
  @Input() myPals: Notification[];
  @Input() myTutors: Notification[];
  @Output() reloadList: EventEmitter<Object> = new EventEmitter();
  buttonText: string = 'Request';

  constructor(
    private userService: UserService,
    private notificationService: NotificationService,
    private snackbar: MatSnackBar
  ) {}

  getTutorRate(course: Course) {
    return course.tutors.find((tutor) => tutor._id === this.user._id).rate;
  }

  isRequestButtonDisabled() {
    let foundItem = null;
    if (this.isPal) {
      foundItem = this.myPals?.find(
        (e) => e.toUser['_id'] === this.user._id || e.fromUser['_id'] === this.user._id
      );
    } else {
      foundItem = this.myTutors?.find(
        (e) => e.toUser['_id'] === this.user._id || e.fromUser['_id'] === this.user._id
      );
    }

    if (this.user._id === this.userService.getUser()._id) {
      this.buttonText = 'You';
      return true;
    }

    if (foundItem) {
      if (foundItem.status === 'accepted') {
        this.buttonText = 'Connected';
      } else {
        this.buttonText = 'Pending';
      }
      return true;
    }
    return false;
  }

  request(course: Course, event: MouseEvent) {
    let newNotification: Notification = {
      toUser: this.user._id,
      fromUser: this.userService.getUser()._id,
      course: { _id: course._id, name: course.name },
      requestType: ''
    };
    if (this.isPal) {
      newNotification.requestType = 'pal';
    } else {
      newNotification.requestType = 'tutor';
    }
    this.notificationService.createNotification(newNotification).subscribe((response: Response) => {
      if (response.success) {
        this.reloadList.emit();
      }
      this.snackbar.open(response.message, 'Remove', {
        duration: 5000
      });
    });
    event.stopPropagation();
  }
}
