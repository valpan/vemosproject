import { Discussion } from './../shared/discussion';
import { DiscussionService } from './../services/discussion.service';
import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DiscussionResolver implements Resolve<Discussion> {
  constructor(private discussionService: DiscussionService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Discussion> {
    return this.discussionService.getDiscussion(route.paramMap.get('id'));
  }
}
