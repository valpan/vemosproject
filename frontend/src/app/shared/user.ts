export interface User {
  _id?: any;
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  password: string;
  description?: string;
  palRatings?: [{ _id: string; rating: number }];
  tutorRatings?: [{ _id: string; rating: number }];
  profilePicture: File;
}
