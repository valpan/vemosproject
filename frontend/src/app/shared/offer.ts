import { Course } from './course';

export interface Offer {
  id: number;
  name?: string;
  role: 'Tutor' | 'Study Buddy';
  rate?: number;
  course: Course;
  rating: number;
}
