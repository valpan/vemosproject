export interface HomepageCardModel {
  iconField?: string;
  pictureField?: Object;
  stringField1: Object;
  stringField2?: Object;
  dateField?: Object;
}
