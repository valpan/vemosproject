import { Course } from './course';
import { University } from './university';

export interface Request {
  id: number;
  picture?: Object;
  name: string;
  role: 'Tutor' | 'Tutee' | 'Pal';
  course: Course;
  status?: 'Accepted' | 'Pending' | 'Rejected' | 'None';
}
