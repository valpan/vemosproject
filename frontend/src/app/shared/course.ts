import { Discussion } from './discussion';
import { University } from './university';

export interface Course {
  _id: string;
  name: string;
  university: University;
  members?: { _id: string; joinDate: Date }[];
  pals?: { _id: string; joinDate: Date }[];
  tutors?: { _id: string; joinDate: Date; rate: number }[];
  discussions?: Discussion[];
  joinDate?: Date;
}
