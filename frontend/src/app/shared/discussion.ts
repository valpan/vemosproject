import { User } from './user';
import { Course } from './course';

export interface Discussion {
  _id?: string;
  title: string;
  questionText?: string;
  creator?: User;
  creationDate?: Date;
  course: Course | string;
  likes?: Number;
  likedBy?: string[];
  commentsTotal?: Number;
  comments?: Object[];
}
