export interface Response {
  success: boolean;
  message?: string;
  objectId?: string;
  object?: any;
}
