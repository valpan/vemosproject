export interface University {
  id?: number;
  name?: string;
  logoName?: string;
}
