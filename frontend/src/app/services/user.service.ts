import { Response } from '../shared/response';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError, Subject } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { Request } from '../shared/request';
import { User } from './../shared/user';
import { environment } from 'src/environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';

const USER = '/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private requestsUrl: string = 'api/requests';
  authToken: any;
  user: User;
  data: any;
  message: string = '';
  componentMethodCallSource = new Subject<any>();
  notificationMethodCalled = this.componentMethodCallSource.asObservable();

  constructor(private http: HttpClient, private router: Router, private snackbar: MatSnackBar) {}

  private handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
      this.snackbar.open(errorMessage, 'Remove', {
        duration: 5000
      });
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      this.snackbar.open(errorMessage, 'Remove', {
        duration: 5000
      });
    }
    return throwError(errorMessage);
  }

  login(email, password) {
    const loginData = { email, password };
    this.http.post(`${environment.APIEndpoint}${USER}/authenticate`, loginData).subscribe(
      (resp) => {
        this.data = resp;
        this.user = this.data.object;
        localStorage.setItem('user', JSON.stringify(this.user));
        localStorage.setItem('jwtToken', this.data.token);
        this.router.navigate(['homepage']);
        this.componentMethodCallSource.next();
        this.snackbar.open(
          `Welcome ${this.user.firstName}. You have successfully logged in.`,
          'Remove',
          {
            duration: 5000
          }
        );
      },
      (err) => {
        this.snackbar.open(err.error.msg, 'Remove', {
          duration: 5000
        });
        this.message = err.error.msg;
      }
    );
  }

  logout() {
    localStorage.clear();
    this.user = null;
    this.router.navigate(['/login']);
  }

  signUp(user: User) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(`${environment.APIEndpoint}${USER}/register`, user, { headers });
  }

  getUser() {
    if (!this.user) {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    return this.user;
  }

  updateUser(user: User): Promise<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .put<Response>(`${environment.APIEndpoint}${USER}/updateUser/${user._id}`, user, { headers })
      .pipe(
        map((res) => {
          localStorage.setItem('user', JSON.stringify(user));
          this.snackbar.open(`Profile successfully saved.`, 'Remove', {
            duration: 5000
          });
          return res.success;
        }),
        catchError(this.handleError)
      )
      .toPromise();
  }

  uploadUserImage(formData: FormData) {
    return this.http.post(`${environment.APIEndpoint}${USER}/upload`, formData);
  }

  findUser(userId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(`${environment.APIEndpoint}${USER}/${userId}`, { headers });
  }

  ratePal(palId, ratingUserId, rating) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(
      `${environment.APIEndpoint}${USER}/ratePal/${palId}`,
      { ratingUserId, rating },
      { headers }
    );
  }

  rateTutor(tutorId, ratingUserId, rating) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(
      `${environment.APIEndpoint}${USER}/rateTutor/${tutorId}`,
      { ratingUserId, rating },
      { headers }
    );
  }

  checkIfAccountExists(email: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(
      `${environment.APIEndpoint}${USER}/checkUserByEmail?email=${email}`,
      {
        headers
      }
    );
  }

  resetPassword(userId: string, password: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<Response>(
      `${environment.APIEndpoint}${USER}/resetPassword/${userId}`,
      { password },
      {
        headers
      }
    );
  }
}
