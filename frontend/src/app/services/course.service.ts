import { Response } from './../shared/response';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Course } from '../shared/course';
import { catchError, map } from 'rxjs/operators';
import { University } from '../shared/university';
import { throwError, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';

const COURSE_URL = '/courses';
const UNI_URL = '/universities';

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  constructor(private http: HttpClient, private snackbar: MatSnackBar) {}

  getCourse(id: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(`${environment.APIEndpoint}${COURSE_URL}/${id}`, {
      headers
    });
  }

  getCourses(query: string): Promise<Course[]> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http
      .get<any>(`${environment.APIEndpoint}${COURSE_URL}/?search_query=${query}`, { headers })
      .pipe(
        map((data) => data.courses),
        catchError(this.handleError)
      )
      .toPromise();
  }

  getMyCourses(userId: string): Observable<Response> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(`${environment.APIEndpoint}${COURSE_URL}/myCourses/${userId}`, {
      headers
    });
  }

  getMyCoursesMember(userId: string): Observable<Response> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(
      `${environment.APIEndpoint}${COURSE_URL}/myCoursesMember/${userId}`,
      {
        headers
      }
    );
  }

  getMyCoursesPal(userId: string): Observable<Response> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(
      `${environment.APIEndpoint}${COURSE_URL}/myCoursesPals/${userId}`,
      {
        headers
      }
    );
  }

  getMyCoursesTutor(userId: string): Observable<Response> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(
      `${environment.APIEndpoint}${COURSE_URL}/myCoursesTutors/${userId}`,
      {
        headers
      }
    );
  }

  getRecentlyJoinedCourses(userId: string): Observable<Response> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(
      `${environment.APIEndpoint}${COURSE_URL}/recentlyJoined/${userId}`,
      {
        headers
      }
    );
  }

  getUniList(): Promise<University[]> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http
      .get<any>(`${environment.APIEndpoint}${UNI_URL}/all`)
      .pipe(
        map((data) => data.universities),
        catchError(this.handleError)
      )
      .toPromise();
  }

  joinCourseMember(courseId: string, userId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<Response>(
      `${environment.APIEndpoint}${COURSE_URL}/joinMember/${courseId}`,
      { userId },
      { headers }
    );
  }

  leaveCourseMember(courseId: string, userId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<Response>(
      `${environment.APIEndpoint}${COURSE_URL}/leaveMember/${courseId}`,
      { userId },
      { headers }
    );
  }

  joinCoursePal(courseId: string, userId: string): Promise<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .put<Response>(
        `${environment.APIEndpoint}${COURSE_URL}/joinPal/${courseId}`,
        { userId },
        { headers }
      )
      .pipe(
        map((res) => {
          this.snackbar.open('Successfully submitted pal application', 'Remove', {
            duration: 5000
          });
          return res.success;
        }),
        catchError(this.handleError)
      )
      .toPromise();
  }

  leaveCoursePal(courseId: string, userId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<Response>(
      `${environment.APIEndpoint}${COURSE_URL}/leavePal/${courseId}`,
      { userId },
      { headers }
    );
  }

  joinCourseTutor(courseId: string, userId: string, rate: number): Promise<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .put<Response>(
        `${environment.APIEndpoint}${COURSE_URL}/joinTutor/${courseId}`,
        { userId, rate },
        { headers }
      )
      .pipe(
        map((res) => {
          this.snackbar.open('Successfully submitted tutor application', 'Remove', {
            duration: 5000
          });
          return res.success;
        }),
        catchError(this.handleError)
      )
      .toPromise();
  }

  leaveCourseTutor(courseId: string, userId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<Response>(
      `${environment.APIEndpoint}${COURSE_URL}/leaveTutor/${courseId}`,
      { userId },
      { headers }
    );
  }

  getCoursePals(courseId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(`${environment.APIEndpoint}${COURSE_URL}/${courseId}/pals`, {
      headers
    });
  }

  getCourseTutors(courseId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(`${environment.APIEndpoint}${COURSE_URL}/${courseId}/tutors`, {
      headers
    });
  }

  private handleError(error) {
    let errorMessage = '';
    if (error.error) {
      // client-side error
      errorMessage = `${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
