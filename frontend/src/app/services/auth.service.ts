import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private jwtHelper: JwtHelperService) {}

  public isAuthenticated(): Boolean {
    const token = localStorage.getItem('jwtToken');
    // Check whether the token is expired and return
    // true or false
    if (this.jwtHelper.isTokenExpired(token)) {
      return false;
    }
    return true;
  }
}
