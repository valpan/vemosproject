import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Notification } from './../shared/notification';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { environment } from '../../environments/environment';
import { Response } from '../shared/response';

const NOTIFICATION = '/notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  @Output() updatedEvent = new EventEmitter<boolean>();
  @Output() updateRequest = new EventEmitter<boolean>();

  constructor(private http: HttpClient) {}

  updateNotifications(value: boolean) {
    this.updatedEvent.emit(value);
  }

  updateIncomingRequests(value: boolean) {
    this.updateRequest.emit(value);
  }

  createNotification(notification: Notification) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(`${environment.APIEndpoint}${NOTIFICATION}`, notification, { headers });
  }

  getNotifications(toUser: string): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<any>(`${environment.APIEndpoint}${NOTIFICATION}/${toUser}`, { headers });
  }

  getNotificationsTo(toUser: string): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<any>(`${environment.APIEndpoint}${NOTIFICATION}/to/${toUser}`, {
      headers
    });
  }

  getNotificationsFrom(fromUser: string): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<any>(`${environment.APIEndpoint}${NOTIFICATION}/from/${fromUser}`, {
      headers
    });
  }

  answerNotification(id: String, status: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(
      `${environment.APIEndpoint}${NOTIFICATION}/${id}`,
      { status },
      { headers }
    );
  }

  readNotification(id: String, isRead: boolean) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(
      `${environment.APIEndpoint}${NOTIFICATION}/${id}`,
      { isRead },
      { headers }
    );
  }

  getConnections(userId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(`${environment.APIEndpoint}${NOTIFICATION}/connections/${userId}`, {
      headers
    });
  }

  // Get all pals a user has connected with
  getMyPals(userId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(`${environment.APIEndpoint}${NOTIFICATION}/myPals/${userId}`, {
      headers
    });
  }

  // Get all tutors a user has connected with
  getMyTutors(userId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(`${environment.APIEndpoint}${NOTIFICATION}/myTutors/${userId}`, {
      headers
    });
  }

  // Get all tutees a user teaches as a tutor
  getMyTutees(userId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(`${environment.APIEndpoint}${NOTIFICATION}/myTutees/${userId}`, {
      headers
    });
  }

  // Get all pals a user has connected with in a specific course
  getMyPalsForCourse(userId: string, courseId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(
      `${environment.APIEndpoint}${NOTIFICATION}/myPals/${userId}/course/${courseId}`,
      {
        headers
      }
    );
  }

  // Get all tutors a user has connected with in a specific course
  getMyTutorsForCourse(userId: string, courseId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(
      `${environment.APIEndpoint}${NOTIFICATION}/myTutors/${userId}/course/${courseId}`,
      {
        headers
      }
    );
  }
}
