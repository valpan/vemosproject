import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatBadgeModule } from '@angular/material/badge';

import { AppComponent } from './app.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { LoginComponent } from './components/login/login.component';
import { SearchListComponent } from './components/search-list/search-list.component';
import { DropdownSearchComponent } from './components/dropdown-search/dropdown-search.component';
import { NavigatorComponent } from './components/navigator/navigator.component';
import { DiscussionDetailComponent } from './components/discussion/discussion-detail/discussion-detail.component';
import { HomepageCardComponent } from './components/homepage/homepage-card/homepage-card.component';
import { NamePipe } from './pipes/name.pipe';
import { CreateDiscussionComponent } from './components/discussion/create-discussion/create-discussion.component';
import { AutofocusDirective } from './directives/autofocus.directive';
import { PersonalProfileComponent } from './components/personal-profile/personal-profile.component';
import { RequestCardComponent } from './components/personal-profile/request-card/request-card.component';
import { NotificationsComponent } from './components/navigator/notifications/notifications.component';
import { MyCoursesComponent } from './components/my-courses/my-courses.component';
import { OtherUserProfileComponent } from './components/other-user-profile/other-user-profile.component';
import { JoinedCourseComponent } from './components/course/joined-course/joined-course.component';
import { SelectedCourseComponent } from './components/course/selected-course/selected-course.component';
import { ProfileSettingsComponent } from './components/navigator/profile-settings/profile-settings.component';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { CourseTabComponent } from './components/my-courses/course-tab/course-tab.component';
import { FooterComponent } from './components/footer/footer.component';
import { PalsTutorsComponent } from './components/pals-tutors/pals-tutors.component';
import { PersonCardComponent } from './components/pals-tutors/person-card/person-card.component';
import { OffersTabComponent } from './components/other-user-profile/offers-tab/offers-tab.component';
import { OfferCardComponent } from './components/course/joined-course/offer-card/offer-card.component';
import { ForgotPasswordComponent } from './components/login/forgot-password/forgot-password.component';
import { QuestionCardComponent } from './components/discussion/discussion-detail/question-card/question-card.component';
import { AnswerCardComponent } from './components/discussion/discussion-detail/answer-card/answer-card.component';
import { WhatsUnipalComponent } from './components/login/whats-unipal/whats-unipal.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchListComponent,
    DropdownSearchComponent,
    LoginComponent,
    NavigatorComponent,
    HomepageComponent,
    NotificationsComponent,
    SelectedCourseComponent,
    JoinedCourseComponent,
    OtherUserProfileComponent,
    DiscussionDetailComponent,
    HomepageCardComponent,
    NamePipe,
    CreateDiscussionComponent,
    AutofocusDirective,
    PersonalProfileComponent,
    RequestCardComponent,
    PalsTutorsComponent,
    MyCoursesComponent,
    CourseTabComponent,
    FooterComponent,
    ProfileSettingsComponent,
    CourseTabComponent,
    PersonCardComponent,
    OfferCardComponent,
    OffersTabComponent,
    ForgotPasswordComponent,
    QuestionCardComponent,
    AnswerCardComponent,
    WhatsUnipalComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatFormFieldModule,
    NgxMatSelectSearchModule,
    MatIconModule,
    MatTabsModule,
    MatCardModule,
    NgbModule,
    MatInputModule,
    MatButtonModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatTabsModule,
    MatMenuModule,
    MatButtonModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatBadgeModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
