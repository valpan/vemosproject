const express = require("express");
const cors = require("cors");
const path = require("path");
const mongoose = require("mongoose");
const config = require("./config");
const passport = require("passport");

// ----- Server ---------------
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Set static folder
app.use(express.static(path.join(__dirname, "public")));

// Set authentication requirements
app.use(passport.initialize());

// API Routes/Calls
app.use("/api/user", require("./routes/user"));
app.use("/api/courses", require("./routes/course"));
app.use("/api/universities", require("./routes/university"));
app.use("/api/discussion", require("./routes/discussion"));
app.use("/api/notification", require("./routes/notification"));

// Homepage
app.get("api/", (req, res) => {
  res.send("Server Home Endpoint");
});

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "public/index.html"));
});

// ----- Database ---------------
// Databse Connection
mongoose.connect(config.database, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

// Connection Established
mongoose.connection.on("connected", () => {
  console.log("Connected to database: " + config.database);
});

// Connection Error
mongoose.connection.on("error", (err) => {
  console.log("Database error " + err);
});

// Port Number
const PORT = process.env.PORT || 5500;

app.listen(PORT, () => {
  console.log("Server started on port " + PORT);
});
