const express = require("express");
const router = express.Router();
const Notification = require("../models/notification");
const passport = require("passport");

//created new notification
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    const newNotification = new Notification({
      toUser: req.body.toUser,
      fromUser: req.body.fromUser,
      course: req.body.course,
      status: req.body.status ? req.body.status : "pending",
      requestType: req.body.requestType,
      discussionId: req.body.discussionId,
    });
    Notification.createNotification(newNotification, (err, notification) => {
      if (err) {
        res.json({ success: false, message: "Request could not be sent" });
      } else {
        res.json({
          success: true,
          message: "Request successfully sent",
          objectId: notification,
        });
      }
    });
  }
);

//Update exisitng notif...
router.put(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Notification.findByIdAndUpdate(
      req.params.id,
      {
        status: req.body.status,
        responseDate: Date.now(),
        isRead: true,
      },
      { new: true },
      (err, result) => {
        res.json(result);
      }
    );
  }
);

router.get(
  "/:toUser",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Notification.getNotificationByToUser(req.params.toUser, (err, result) => {
      if (err) {
        res.json({ success: false, message: "Failed to find notifications" });
      } else if (result.length > 0) {
        res.json({ objectArray: result });
      } else {
        res.json({
          success: true,
          message:
            "Request successful but no results found for " + req.params.toUser,
        });
      }
    });
  }
);

router.get(
  "/from/:fromUser",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Notification.getNotificationFromUser(req.params.fromUser, (err, result) => {
      if (err) {
        res.json({ success: false, message: "Failed to find notifications" });
      } else if (result.length > 0) {
        res.json({ objectArray: result });
      } else {
        res.json({
          success: true,
          message:
            "Request successful but no results found for " +
            req.params.fromUser,
        });
      }
    });
  }
);

router.get(
  "/to/:toUser",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Notification.getNotificationToUser(req.params.toUser, (err, result) => {
      if (err) {
        res.json({ success: false, message: "Failed to find notifications" });
      } else if (result.length > 0) {
        res.json({ objectArray: result });
      } else {
        res.json({
          success: true,
          message:
            "Request successful but no results found for " + req.params.toUser,
        });
      }
    });
  }
);

router.get(
  "/connections/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Notification.getConnections(req.params.id, (err, result) => {
      if (err) {
        console.log(err);
        res.json({ success: false, message: "Failed to find connections" });
      } else if (result.length > 0) {
        res.json({
          success: true,
          message: "Successfully found connections",
          object: result,
        });
      } else {
        res.json({
          success: false,
          message: "No results found for you",
        });
      }
    });
  }
);

// Get all pals a user has connected with
router.get(
  "/myPals/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Notification.getMyPals(req.params.id, (err, result) => {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          message: "Failed to find connections by pal",
        });
      } else if (result.length > 0) {
        res.json({
          success: true,
          message: "Successfully found pal connections",
          object: result,
        });
      } else {
        res.json({
          success: false,
          message: "No results found for you",
        });
      }
    });
  }
);

// Get all tutors a user has connected with
router.get(
  "/myTutors/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Notification.getMyTutors(req.params.id, (err, result) => {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          message: "Failed to find connections by tutor",
        });
      } else if (result.length > 0) {
        res.json({
          success: true,
          message: "Successfully found tutor connections",
          object: result,
        });
      } else {
        res.json({
          success: false,
          message: "No results found for you",
        });
      }
    });
  }
);

// Get all tutees a user teaches as a tutor
router.get(
  "/myTutees/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Notification.getMyTutees(req.params.id, (err, result) => {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          message: "Failed to find connections by tutor",
        });
      } else if (result.length > 0) {
        res.json({
          success: true,
          message: "Successfully found tutor connections",
          object: result,
        });
      } else {
        res.json({
          success: false,
          message: "No results found for you",
        });
      }
    });
  }
);

// Get all pals a user has connected with in a specific course
router.get(
  "/myPals/:id/course/:courseId",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Notification.getMyPalsForCourse(
      req.params.id,
      req.params.courseId,
      (err, result) => {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            message: "Failed to find pals for a course",
          });
        } else if (result.length > 0) {
          res.json({
            success: true,
            message: "Successfully found pal connections for a course",
            object: result,
          });
        } else {
          res.json({
            success: false,
            message: "No results found for you",
          });
        }
      }
    );
  }
);

// Get all tutors a user has connected with in a specific course
router.get(
  "/myTutors/:id/course/:courseId",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Notification.getMyTutorsForCourse(
      req.params.id,
      req.params.courseId,
      (err, result) => {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            message: "Failed my tutors for a course",
          });
        } else if (result.length > 0) {
          res.json({
            success: true,
            message: "Successfully found tutor connections for a course",
            object: result,
          });
        } else {
          res.json({
            success: false,
            message: "No results found for you",
          });
        }
      }
    );
  }
);

module.exports = router;
