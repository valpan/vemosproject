const express = require("express");
const router = express.Router();
const Course = require("../models/course");
const passport = require("passport");
require("../passport")(passport);

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    const searchQuery = req.query.search_query;
    Course.getCourseByName(searchQuery, (err, courses) => {
      if (err) {
        console.log("error", err);
        res.json({ success: false, message: "Something went wrong..." });
      } else if (courses.length === 0) {
        res
          .status(404)
          .json({
            success: false,
            message: "No course with that name was found",
            courses: courses,
          });
      } else {
        res.json({
          success: true,
          message: "One or many courses successfully found",
          courses: courses,
        });
      }
    });
  }
);

router.get(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.getCourseById(req.params.id, (err, course) => {
      if (err) {
        res.json({ success: false, message: "Error when finding course" });
      } else if (course) {
        res.json({
          success: true,
          message: "Course found",
          object: course,
        });
      } else {
        res.json({ success: false, message: "Course not found" });
      }
    });
  }
);

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.createCourse(req.body.name, req.body.universityId, (err, course) => {
      if (err) {
        res.json({ success: false, message: "Creating the course failed" });
      } else {
        res.json({
          success: true,
          message: "Course creation success",
          object: course,
        });
      }
    });
  }
);

router.put(
  "/joinMember/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.findByIdAndUpdate(
      req.params.id,
      { $push: { members: { _id: req.body.userId } } },
      { new: true }
    )
      .populate("university")
      .exec((err, course) => {
        if (err) {
          res.json({
            success: false,
            message: "Failed to join user as a member",
          });
        } else {
          res.json({
            success: true,
            message: "Successfully joined user to course as a member",
            object: course,
          });
        }
      });
  }
);

router.put(
  "/leaveMember/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.findByIdAndUpdate(
      req.params.id,
      { $pull: { members: { _id: req.body.userId } } },
      { new: true }
    )
      .populate("university")
      .exec((err, course) => {
        if (err) {
          res.json({
            success: false,
            message: "Failed to remove user as a member",
          });
        } else {
          res.json({
            success: true,
            message: "Successfully remove user to course as a member",
            object: course,
          });
        }
      });
  }
);

router.put(
  "/joinPal/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.findByIdAndUpdate(
      req.params.id,
      { $push: { pals: { _id: req.body.userId } } },
      { new: true },
      (err, course) => {
        if (err) {
          res.json({ success: false, message: "Failed to join user as a pal" });
        } else {
          res.json({
            success: true,
            message: "Successfully joined user to course as a pal",
            object: course,
          });
        }
      }
    );
  }
);

router.put(
  "/leavePal/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.findByIdAndUpdate(
      req.params.id,
      { $pull: { pals: { _id: req.body.userId } } },
      { new: true },
      (err, course) => {
        if (err) {
          res.json({
            success: false,
            message: "Failed to remove user as a pal",
          });
        } else {
          res.json({
            success: true,
            message: "Successfully removed user from course as a pal",
            object: course,
          });
        }
      }
    );
  }
);

router.put(
  "/joinTutor/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.findByIdAndUpdate(
      req.params.id,
      { $push: { tutors: { _id: req.body.userId, rate: req.body.rate } } },
      { new: true },
      (err, course) => {
        if (err) {
          res.json({
            success: false,
            message: "Failed to join user as a tutor",
          });
        } else {
          res.json({
            success: true,
            message: "Successfully joined user to course as a tutor",
            object: course,
          });
        }
      }
    );
  }
);

router.put(
  "/leaveTutor/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.findByIdAndUpdate(
      req.params.id,
      { $pull: { tutors: { _id: req.body.userId } } },
      { new: true },
      (err, course) => {
        if (err) {
          res.json({
            success: false,
            message: "Failed to remove user as a tutor",
          });
        } else {
          res.json({
            success: true,
            message: "Successfully removed user to course as a tutor",
            object: course,
          });
        }
      }
    );
  }
);

router.get(
  "/myCourses/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.findAllmyCoursesByUserId(req.params.id, (err, courses) => {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          message: "Error when finding courses",
          err,
        });
      } else if (courses.length > 0) {
        res.json({
          success: true,
          message: "Courses found",
          object: courses,
        });
      } else {
        res.json({ success: false, message: "Courses not found" });
      }
    });
  }
);

router.get(
  "/myCoursesMember/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.findCoursesForMemberByUserId(req.params.id, (err, courses) => {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          message: "Error when finding courses",
          err,
        });
      } else if (courses.length > 0) {
        res.json({
          success: true,
          message: "Courses found",
          object: courses,
        });
      } else {
        res.json({ success: false, message: "Courses not found" });
      }
    });
  }
);

router.get(
  "/myCoursesPals/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.findCoursesForPalsByUserId(req.params.id, (err, courses) => {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          message: "Error when finding courses",
          err,
        });
      } else if (courses.length > 0) {
        res.json({
          success: true,
          message: "Courses found",
          object: courses,
        });
      } else {
        res.json({ success: false, message: "Courses not found" });
      }
    });
  }
);

router.get(
  "/myCoursesTutors/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.findCoursesForTutorsByUserId(req.params.id, (err, courses) => {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          message: "Error when finding courses",
          err,
        });
      } else if (courses.length > 0) {
        res.json({
          success: true,
          message: "Courses found",
          object: courses,
        });
      } else {
        res.json({ success: false, message: "Courses not found" });
      }
    });
  }
);

router.get(
  "/recentlyJoined/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.findRecentlyJoinedMember(req.params.id, (err, courses) => {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          message: "Error when finding courses",
          err,
        });
      } else if (courses.length > 0) {
        res.json({
          success: true,
          message: "Courses found",
          object: courses,
        });
      } else {
        res.json({ success: false, message: "Courses not found" });
      }
    });
  }
);

// Finding all pals for a course
router.get(
  "/:id/pals",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.findById(req.params.id, { _id: 0, pals: 1 })
      .populate({ path: "pals._id" })
      .exec((err, results) => {
        if (err) {
          console.log(err);
          res.json({ success: false, message: "Failed to find course" });
        } else if (results.pals.length > 0) {
          res.json({
            success: true,
            message: "Found pals",
            object: results.pals,
          });
        } else {
          res.json({ success: false, message: "Pals not found" });
        }
      });
  }
);

// Finding all tutors for a course
router.get(
  "/:id/tutors",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Course.findById(req.params.id, { _id: 0, tutors: 1 })
      .populate({ path: "tutors._id" })
      .exec((err, results) => {
        if (err) {
          console.log(err);
          res.json({ success: false, message: "Failed to find course" });
        } else if (results.tutors.length > 0) {
          res.json({
            success: true,
            message: "Found pals",
            object: results.tutors,
          });
        } else {
          res.json({ success: false, message: "Tutors not found" });
        }
      });
  }
);

module.exports = router;
