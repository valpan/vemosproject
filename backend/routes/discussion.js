const express = require('express');
const router = express.Router();
const Discussion = require('../models/discussion');
const Course = require('../models/course');
const passport = require('passport');
require('../passport')(passport);

router.get('/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Discussion.getDiscussionById(req.params.id, (err, result) => {
    if (err) {
      res.json({ success: false, message: 'Failed to find discussion' });
    } else {
      res.json(result[0]);
    }
  });
});

router.post('/', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  const newDiscussion = new Discussion({
    title: req.body.title,
    questionText: req.body.questionText,
    course: req.body.course,
    creator: req.body.creator._id,
    creationDate: new Date(),
    commnets: [],
    commentsTotal: 0,
    likes: 1,
    likedBy: [req.body.creator._id]
  });

  Discussion.createDiscussion(newDiscussion, (err, discussion) => {
    if (err) {
      res.json({ success: false, message: 'Creating the discussion failed' });
    } else {
      Course.findByIdAndUpdate(
        newDiscussion.course,
        {
          $push: { discussions: { _id: discussion._id } }
        },
        (err, course) => {
          if (err) {
            res.json({ success: false, message: 'Creating the discussion failed' });
          } else {
            res.json({
              success: true,
              message: 'Discussion creation success',
              objectId: discussion._id
            });
          }
        }
      );
    }
  });
});

router.put('/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Discussion.findByIdAndUpdate(
    req.params.id,
    {
      questionText: req.body.questionText
    },
    { new: true },
    (err, updatedDiscussion) => {
      if (err) {
        res.json({ success: false, message: 'Failed to update discussion' });
      } else {
        res.json({
          success: true,
          message: 'Updated succesfully',
          objectId: updatedDiscussion._id
        });
      }
    }
  );
});

router.delete('/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Discussion.findByIdAndDelete(req.params.id, (err) => {
    if (err) {
      res.json({ success: false, message: 'Failed to delete discussion' });
    } else {
      res.json({ success: true, message: 'Deleted succesfully' });
    }
  });
});

router.put('/like/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Discussion.findById(req.params.id, (err, discussion) => {
    if (err) {
      console.log('err', err);
    } else {
      if (discussion.likedBy.includes(req.body.userId)) {
        discussion.likes -= 1;
        discussion.likedBy.pull(req.body.userId);
      } else {
        discussion.likes += 1;
        discussion.likedBy.push(req.body.userId);
      }
      discussion.save((err, updatedDiscussion) => {
        if (err) {
          res.json({ success: false, message: 'Failed to like discussion' });
        } else {
          Discussion.populate(
            updatedDiscussion,
            [{ path: 'creator' }, { path: 'course' }],
            (err, populatedDiscussion) => {
              if (err) {
                res.json({
                  success: false,
                  message: 'Failed to like comment (populate discusison)'
                });
              } else {
                res.json({
                  success: true,
                  message: 'Liked discussion succesfully',
                  object: populatedDiscussion
                });
              }
            }
          );
        }
      });
    }
  });
});

router.post('/comment', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  const newComment = {
    text: req.body.commentText,
    creator: {
      _id: req.body.commentCreator.id,
      firstName: req.body.commentCreator.firstName,
      lastName: req.body.commentCreator.lastName,
      username: req.body.commentCreator.username
    },
    creationDate: new Date(),
    likes: 1,
    likedBy: [req.body.commentCreator.id]
  };
  Discussion.findById(req.body.discussionId, (err, discussion) => {
    if (err) {
      console.log('err', err);
    } else {
      discussion.comments.push(newComment);
      discussion.commentsTotal += 1;
      discussion.save((err, updatedDiscussion) => {
        if (err) {
          res.json({
            success: false,
            message: 'Failed to save discussion comment'
          });
        } else {
          Discussion.populate(
            updatedDiscussion,
            [{ path: 'creator' }, { path: 'course' }],
            (err, populatedDiscussion) => {
              if (err) {
                res.json({
                  success: false,
                  message: 'Failed to post comment'
                });
              } else {
                res.json({
                  success: true,
                  message: 'Comment saved successfully',
                  object: populatedDiscussion
                });
              }
            }
          );
        }
      });
    }
  });
});

router.put(
  '/editComment/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res, next) => {
    Discussion.findById(req.params.id, (err, discussion) => {
      if (err) {
        console.log('err', err);
      } else {
        discussion.comments.id(req.body.commentId).text = req.body.commentText;
        discussion.save((err, updatedDiscussion) => {
          if (err) {
            res.json({ success: false, message: 'Failed to edit comment' });
          } else {
            Discussion.populate(
              updatedDiscussion,
              [{ path: 'creator' }, { path: 'course' }],
              (err, populatedDiscussion) => {
                if (err) {
                  res.json({
                    success: false,
                    message: 'Failed to edit comment (populate discusison)'
                  });
                } else {
                  res.json({
                    success: true,
                    message: 'Edited comment succesfully',
                    object: populatedDiscussion
                  });
                }
              }
            );
          }
        });
      }
    });
  }
);

router.put(
  '/deleteComment/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res, next) => {
    Discussion.findById(req.params.id, (err, discussion) => {
      if (err) {
        console.log('err', err);
      } else {
        discussion.comments.id(req.body.commentId).remove();
        discussion.commentsTotal -= 1;
        discussion.save((err, updatedDiscussion) => {
          if (err) {
            res.json({ success: false, message: 'Failed to delete comment' });
          } else {
            Discussion.populate(
              updatedDiscussion,
              [{ path: 'creator' }, { path: 'course' }],
              (err, populatedDiscussion) => {
                if (err) {
                  res.json({
                    success: false,
                    message: 'Failed to delete comment (populate discusison)'
                  });
                } else {
                  res.json({
                    success: true,
                    message: 'Deleted comment succesfully',
                    object: populatedDiscussion
                  });
                }
              }
            );
          }
        });
      }
    });
  }
);

router.put(
  '/likeComment/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res, next) => {
    Discussion.findById(req.params.id, (err, discussion) => {
      if (err) {
        err;
      } else {
        if (discussion.comments.id(req.body.commentId).likedBy.includes(req.body.userId)) {
          discussion.comments.id(req.body.commentId).likes -= 1;
          discussion.comments.id(req.body.commentId).likedBy.pull(req.body.userId);
        } else {
          discussion.comments.id(req.body.commentId).likes += 1;
          discussion.comments.id(req.body.commentId).likedBy.push(req.body.userId);
        }
        discussion.save((err, updatedDiscussion) => {
          if (err) {
            res.json({ success: false, message: 'Failed to like comment' });
          } else {
            Discussion.populate(
              updatedDiscussion,
              [{ path: 'creator' }, { path: 'course' }],
              (err, populatedDiscussion) => {
                if (err) {
                  res.json({
                    success: false,
                    message: 'Failed to like comment (populate discusison)'
                  });
                } else {
                  res.json({
                    success: true,
                    message: 'Liked comment succesfully',
                    object: populatedDiscussion
                  });
                }
              }
            );
          }
        });
      }
    });
  }
);
// Get all questions posted by a user
router.get('/user/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Discussion.getDiscussionsByUser(req.params.id, (err, result) => {
    if (err) {
      res.json({ success: false, message: 'Failed to find discussions for user' });
    } else {
      res.json(result);
    }
  });
});

router.get('/course/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Discussion.getDiscussionsByCourse(req.params.id, (err, result) => {
    if (err) {
      res.json({ success: false, message: 'Failed to find discussions for course' });
    } else if (result.length > 0) {
      res.json({ success: true, message: 'Discussions by course found', object: result });
    } else {
      res.json({ success: false, message: 'Failed to find discussions for course' });
    }
  });
});

router.get(
  '/user/:userId/course/:courseId',
  passport.authenticate('jwt', { session: false }),
  (req, res, next) => {
    Discussion.getDiscussionsByUserAndCourse(
      req.params.userId,
      req.params.courseId,
      (err, result) => {
        if (err) {
          res.json({ success: false, message: 'Failed to find discussions for user and course' });
        } else {
          res.json(result);
        }
      }
    );
  }
);

module.exports = router;
