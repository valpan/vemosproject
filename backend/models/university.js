const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const config = require("../config");

//Uni Schema
const UniSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  logoName: {
    type: String,
  },
});

const Uni = (module.exports = mongoose.model("University", UniSchema));

module.exports.getCourseById = function (id, callback) {
  Uni.findById(id, callback);
};

module.exports.getAllUniversities = function (callback) {
  Uni.find(callback);
};

module.exports.createUniversity = function (name, callback) {
  const newUni = new Uni({
    name: name,
  });
  newUni.save(callback);
};
